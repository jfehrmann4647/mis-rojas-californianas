<!DOCTYPE php>
<?php
    include '../../db-connect/conection.php';
    if(!$dbconn){
        echo '¡Ups!, a ocurido algun error, comunicece con el administrador de la pagina';
        die();
    }
?>


<html>
	<head lang="es">
        <title>Mis Rojas Californianas</title>
        <script src="../../jq/jquery.js"></script>
	    <link rel="stylesheet" href="../../css/style.css"/>
	    <link rel="icon" href="../../img/icon-icon.png"/>
	    <meta charset="UTF-8">
		<meta name="viewport" content="width=divice-width, use-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	</head>
	<body>
	    <div class="main">
	        <?php include '../../navbar-nav.html'?>
	        <div class="paper-body">
	            <div class="conteiner" style="background-image: none;">
	                <div class="wall-paper">
                        <div class="search_filter">
                            <label id=filter>Buscar:
                                <select id="select-filter">
                                    <option <?php if(!isset($_GET['search'])){echo 'id="get-selected"';}?> >Unidades (U.)</option>
                                    <?php
                                        if(isset($_GET['search'])){$formato=$_GET['search'];}
                                        $search_filter_formato=mysqli_query($dbconn, "SELECT mrc_formato_producto FROM mrc_producto WHERE mrc_codigo_tipo_producto='P-002'");
                                        while($row_formato=mysqli_fetch_array($search_filter_formato)){
                                            if($row_formato['mrc_formato_producto']==$formato){
                                                echo '<option selected id="get-selected">'.$row_formato['mrc_formato_producto'].'</option>';
                                            } 
                                            else{
                                                echo '<option>'.$row_formato['mrc_formato_producto'].'</option>';
                                            }
                                           
                                        }
                                    ?>
                                </select>
                            </label>
                        </div>
	                    <?php
                              $search_filter=mysqli_query($dbconn, "SELECT mrc_nombre_producto, mrc_precio_producto, mrc_descuento_producto, mrc_formato_producto, mrc_promocion_producto, mrc_img_producto, mrc_descripcion_producto FROM mrc_producto WHERE mrc_codigo_tipo_producto='P-002'");
                              while($row=mysqli_fetch_array($search_filter)){
                                  echo '<div class="info-content-1" id="'.$row['mrc_formato_producto'].'">    
                                            <div class="col-3">
                                                <img src="../../img/'.$row['mrc_img_producto'].'" width="200px">
                                            </div>
                                            <div class="col-9">
                                                <p>Nombre: '.$row['mrc_nombre_producto'].'</p>
                                                <p>Precio: $'.$row['mrc_precio_producto'].'</p>
                                                <p>Cantidad: '.$row['mrc_formato_producto'].'</p>
                                                <p>Descripcion: '.$row['mrc_descripcion_producto'].'</p>
                                            </div>
                                        </div>';
                              }
                          ?>
	                </div>
	            </div>
	        </div>
	    </div>
        <script>
            $(document).ready(function(){
                 $('.navbar-menu .navbar-nav li:has(ul)').click(function(e){
                e.preventDefault();
                
                if($(this).hasClass('Active')){
                    $(this).removeClass('Active');
                    $(this).children('ul').slideUp();
                }
                else{
                    $('.navbar-menu .navbar-nav li').removeClass('Active');
                    $('.navbar-menu .navbar-nav li ul').slideUp();
                    $(this).addClass('Active');
                    $(this).children('ul').slideDown();
                }
            });
            
            $('.navbar-menu .navbar-body .navbar-nav li ul li a').click(function(){
                window.location.href=$(this).attr("href"); 
            });
                
                $('.navbar-menu .navbar-body .navbar-responsive').click(function(e){
                e.preventDefault();
                
                if($('.navbar-menu .navbar-body .navbar-nav').hasClass("Responsive-Active")){
                    $('.navbar-menu .navbar-body .navbar-nav').removeClass("Responsive-Active");
                    $('.navbar-menu .navbar-body .navbar-nav').slideUp();
                }
                else{
                    $('.navbar-menu .navbar-body .navbar-nav').addClass("Responsive-Active");
                    $('.navbar-menu .navbar-body .navbar-nav').slideDown();
                }
            });

                $('.social-menu').mouseenter(function(){
                    $(this).css("left", "-10px");
                    $(this).css("opacity", 1);
                    $('.social-menu .icon-social .test').css("left", "20px");
                    $('.social-menu .icon-social .test span').css("display", "inline");
                });

                $('.social-menu').mouseleave(function(){
                    $(this).css("left", "-250px");
                    $(this).css("opacity", 0.5);
                    $('.social-menu .icon-social .test').css("left", "280px");
                    $('.social-menu .icon-social .test span').css("display", "none");
                });

                $('.social-menu .icon-social .test a').mouseenter(function(){
                    $(this).css("padding-left", "30px");
                });

                $('.social-menu .icon-social .test a').mouseleave(function(){
                    $(this).css("padding-left", "0px");
                });
                
                
                
                var get_value=$('#select-filter #get-selected').val();
                if(get_value!="Unidades (U.)"){
                    $('.info-content-1').css("display", "none");
                    $('.info-content-1').each(function(){
                         if($(this).attr("id")==get_value){
                                 $(this).css("display", "");
                         }
                    });
                }
                $('select#select-filter').on('change', function(){
                    get_value=$(this).val();
                    $('.info-content-1').css("display", "none");
                    if(get_value!="Unidades (U.)"){
                        $('.info-content-1').each(function(){
                            if($(this).attr("id")==get_value){
                                 $(this).css("display", "");
                            }
                        });
                    }
                    else{
                        $('.info-content-1').css("display", "");
                    }
                    
                });
            });
        </script>
    </body>
    
<style>
    .paper-body .conteiner .wall-paper .search_filter{
        background: linear-gradient(to bottom, #AAF 75%, #FFF 100%);
        padding: 20px;
        padding-bottom: 50px;
        color: #555;
    }
    
    .paper-body .conteiner .wall-paper .search_filter #select-filter {
        color: #444;
        line-height: 1.3;
        padding: .4em 1.4em .3em .8em;
        width: 200px;
        box-sizing: border-box;
        border: 1px solid #aaa;
        box-shadow: 0 1px 0 1px rgba(0,0,0,.03);
        border-radius: .3em;
        -moz-appearance: none;
        -webkit-appearance: none;
        appearance: none;
        background-color: #fff;
        background-image: url('data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22292.4%22%20height%3D%22292.4%22%3E%3Cpath%20fill%3D%22%23007CB2%22%20d%3D%22M287%2069.4a17.6%2017.6%200%200%200-13-5.4H18.4c-5%200-9.3%201.8-12.9%205.4A17.6%2017.6%200%200%200%200%2082.2c0%205%201.8%209.3%205.4%2012.9l128%20127.9c3.6%203.6%207.8%205.4%2012.8%205.4s9.2-1.8%2012.8-5.4L287%2095c3.5-3.5%205.4-7.8%205.4-12.8%200-5-1.9-9.2-5.5-12.8z%22%2F%3E%3C%2Fsvg%3E'),
         linear-gradient(to bottom, #ffffff 0%,#f7f7f7 100%);
        background-repeat: no-repeat, repeat;
        background-position: right .7em top 50%, 0 0;
        background-size: .65em auto, 100%;
    }
    .paper-body .conteiner .wall-paper .search_filter #select-filter::-ms-expand {
        display: none;
    }
    .paper-body .conteiner .wall-paper .search_filter #select-filter:hover {
        border-color: #888;
    }
    .paper-body .conteiner .wall-paper .search_filter #select-filter:focus {
        border-color: #aaa;
        box-shadow: 0 0 1px 3px rgba(59, 153, 252, .7);
        box-shadow: 0 0 0 3px -moz-mac-focusring;
        color: #222; 
        outline: none;
    }

    
    .paper-body .conteiner .wall-paper .search_filter #filter{
        position: relative;
        float: right;
    }
    
    .paper-body .conteiner .wall-paper .info-content-1{
        background: #FFF;
    }
    
    .paper-body .conteiner .wall-paper .info-content-1 .col-9{
        color: #00F;
    }

</style>