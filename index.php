<!DOCTYPE php>
<?php
    include 'db-connect/conection.php';
    if(!$dbconn){
        echo '¡Ups!, a ocurido algun error, comunicece con el administrador de la pagina';
        die();
    }
?>

<html>
	<head lang="es">
        <title>Mis Rojas Californianas</title>
        <script src="jq/jquery.js"></script>
	    <link rel="stylesheet" href="css/style.css"/>
	    <link rel="stylesheet" href="css/responsive.css"/>
	    <link rel="icon" href="img/icon-icon.PNG"/>
	    <meta charset="UTF-8">
		<meta name="viewport" content="width=divice-width, use-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	</head>
	<body>
       <div class="main">
            <div class=""></div>
            <div class="navbar-menu">
              <div class="navbar-header">
                  <p>Por un planeta mas limpio</p>
              </div>
                <div class="navbar-body"><a href="#" class="navbar-responsive">Menu<img src="img/icon-menu.png"/></a>
                   <div class="navbar-logo">
                       <a href="index.php" style="position: relative; left: 20px;"><img src="img/img-logo.png" width="238px"/></a>
                   </div>
                    <ul class="navbar-nav">
                       <li><a href="index.php">Inicio</a></li
                        ><li><a href="#">Productos <img src="img/arrow-expand.png" width=15px/></a>
                           <ul>
                               <li><a href="html-php/mostrar-db/product-Tierra.php">Tierra</a></li>
                               <li><a href="html-php/mostrar-db/product-Lombrices.php">Lombrices</a></li>
                               <li><a href="html-php/mostrar-db/product-Liquidos.php">Liquidos</a></li>
                           </ul>
                       </li
                       ><li><a href="#">Acerca de <img src="img/arrow-expand.png" width=15px/></a>
                           <ul>
                               <li><a href="#">Nuestra empresa</a></li>
                               <li><a href="#">Medios de pago</a></li>
                               <li><a href="#">Politicas</a></li>
                           </ul>
                       </li
                       ><li><a href="#">Contactanos</a></li>
                   </ul>
                </div>
           </div>
           <div class="paper-body">
               <div class="conteiner">
                   <div class="window">
                       <div class="overflow-button">
                           <span style="position: relative; right: 1px; top: 1px;">V</span>
                        </div>
                   </div>
                   <div class="wall-paper">
                      <div>
                          <p>Este es un texto de prueba, no es nada en espeial pero servira como referencia para seguir con el diseño de la pagina web, ignora este texto si puedes leerlo</p>
                          <p>Este es un texto de prueba, no es nada en espeial pero servira como referencia para seguir con el diseño de la pagina web, ignora este texto si puedes leerlo</p>
                          <p>Este es un texto de prueba, no es nada en espeial pero servira como referencia para seguir con el diseño de la pagina web, ignora este texto si puedes leerlo</p>
                          <p>Este es un texto de prueba, no es nada en espeial pero servira como referencia para seguir con el diseño de la pagina web, ignora este texto si puedes leerlo</p>
                          <p>Este es un texto de prueba, no es nada en espeial pero servira como referencia para seguir con el diseño de la pagina web, ignora este texto si puedes leerlo</p>
                          <p>Este es un texto de prueba, no es nada en espeial pero servira como referencia para seguir con el diseño de la pagina web, ignora este texto si puedes leerlo</p>
                      </div>
                      <div class="div-row-1"></div>
                      <div class="info-content-1">
                          <div class="col-3">
                              <img src="img/img-01.jpg" width="400px"/>
                          </div
                          ><div class="col-9">
                              <p>En "Mis Rojas Californianas" damos especial enfacis en el cuidado del medio ambiente, por lo que nuestros productos no poseen ningun tipo de pestisidas y producimos los productos de manera 100% organica </p>

                          </div>
                      </div>
                      <div class="div-row-2"></div>
                      <div class="title-product" style="padding: 20px 80px;"><h1>Productos:</h1></div>
                          <?php
                                $search_filter=mysqli_query($dbconn, "SELECT mrc_codigo_tipo_producto, mrc_nombre_tipo_producto FROM mrc_tipo_producto");
                                while($row1=mysqli_fetch_array($search_filter)){
                                    $count=0;
                                    $cod_product=$row1['mrc_codigo_tipo_producto'];
                                    echo '<div class="box-product"><div class="more-product"><a href="html-php/mostrar-db/product-'.$row1['mrc_nombre_tipo_producto'].'.php">Mostrar mas</a></div><h2>'.$row1['mrc_nombre_tipo_producto'].':</h2><div class="row-product">';
                                    $search_filter2=mysqli_query($dbconn, "SELECT P.mrc_nombre_producto, P.mrc_precio_producto, P.mrc_descuento_producto, P.mrc_promocion_producto, P.mrc_img_producto, P.mrc_formato_producto FROM mrc_producto P, mrc_tipo_producto T WHERE P.mrc_codigo_tipo_producto=T.mrc_codigo_tipo_producto AND T.mrc_codigo_tipo_producto='$cod_product'");
                                    while($row2=mysqli_fetch_array($search_filter2)){
                                        if($count==3){ 
                                            $row2=null;
                                        }
                                        else{
                                            $count++;                                            
                                            echo '<div class="product">
                                                    <a href="html-php/mostrar-db/product-'.$row1['mrc_nombre_tipo_producto'].'.php?search='.$row2['mrc_formato_producto'].'">
                                                        <div class="img-product">
                                                            <img src="img/'.$row2['mrc_img_producto'].'"/>
                                                        </div>
                                                        <div class="price-product">
                                                            <span>Precio: $'.$row2['mrc_precio_producto'].'</span>
                                                            <span>Cantidad: '.$row2['mrc_formato_producto'].'</span>
                                                        </div>
                                                     </a>
                                                  </div>';
                                        }
                                    }
                                    echo '</div>
                                        </div>';
                                }
                          ?>
                       </div>
                       <div class="foother">
                           <p>Pie de Pagina</p>
                       </div>
                   </div>
               </div>
           </div>
       <div class="social-menu">
           <div class="icon-social">
               <div class="test">
                  <a href="https://www.instagram.com/mis_rojas_californianas_"><img src="img/icon-facebook.png" width="40px" height="40px"/><span style="display: none;"> Facebook/Mis Rojas Californeanas</span></a>
                </div>
            </div>
           <div class="icon-social">
               <div class="test">
                   <a href="https://www.instagram.com/mis_rojas_californianas_"><img src="img/icon-instagram.png" width="40px" height="40px"/><span style="display: none;"> Instagram/Mis Rojas Californeanas</span></a>
               </div>
            </div>
           <div class="icon-social">
               <div class="test">
                   <a href="https://www.instagram.com/mis_rojas_californianas_"><img src="img/icon-whatsapp.png" width="40px" height="40px"/><span style="display: none;"> Contactanos: +56964717369</span></a>
                </div>
            </div>
       </div>
    <script>
        $(document).ready(function(){
            $('.navbar-menu .navbar-nav li:has(ul)').click(function(e){
                e.preventDefault();
                
                if($(this).hasClass('Active')){
                    $(this).removeClass('Active');
                    $(this).children('ul').slideUp();
                }
                else{
                    $('.navbar-menu .navbar-nav li').removeClass('Active');
                    $('.navbar-menu .navbar-nav li ul').slideUp();
                    $(this).addClass('Active');
                    $(this).children('ul').slideDown();
                }
            });
            
            $('.navbar-menu .navbar-body .navbar-nav li ul li a').click(function(){
                window.location.href=$(this).attr("href"); 
            });
            
            $('.social-menu').mouseenter(function(){
                $(this).css("left", "-10px");
                $(this).css("opacity", 1);
                $('.social-menu .icon-social .test').css("left", "20px");
                $('.social-menu .icon-social .test span').css("display", "inline");
            });
            
            $('.social-menu').mouseleave(function(){
                $(this).css("left", "-250px");
                $(this).css("opacity", 0.5);
                $('.social-menu .icon-social .test').css("left", "280px");
                $('.social-menu .icon-social .test span').css("display", "none");
            });
            
            $('.navbar-menu .navbar-body .navbar-responsive').click(function(e){
                e.preventDefault();
                
                if($('.navbar-menu .navbar-body .navbar-nav').hasClass("Responsive-Active")){
                    $('.navbar-menu .navbar-body .navbar-nav').removeClass("Responsive-Active");
                    $('.navbar-menu .navbar-body .navbar-nav').slideUp();
                }
                else{
                    $('.navbar-menu .navbar-body .navbar-nav').addClass("Responsive-Active");
                    $('.navbar-menu .navbar-body .navbar-nav').slideDown();
                }
            });
            
            $('.social-menu .icon-social .test a').mouseenter(function(){
                $(this).css("padding-left", "30px");
            });
            
            $('.social-menu .icon-social .test a').mouseleave(function(){
                $(this).css("padding-left", "0px");
            });
            
            $('.paper-body .conteiner .wall-paper .box-product .more-product a').mouseenter(function(){
                $(this).parent('div').parent('div').css("box-shadow", "0px 0px 10px 0px #666");
            });
            
            $('.paper-body .conteiner .wall-paper .box-product .more-product a').mouseleave(function(){
                $(this).parent('div').parent('div').css("box-shadow", "0px 0px 0px 0px #666");
            });
            
            $('.paper-body .conteiner .window .overflow-button').click(function(){
                var ScrollPosition = $('.paper-body .conteiner').scrollTop();
                console.log(ScrollPosition);
                $('.paper-body .conteiner').animate({
                    scrollTop: 480
                }, 500);
            });
        });
    </script>
    </body>	
</html>

<style>
    
</style>